/******************************************************************************/
/*                                                                            */
/* AutoScroll.c                                                               */
/* Quinn-Curtis                                                               */
/* 04.02.22                                                                   */
/*                                                                            */
/******************************************************************************/

#include <windows.h>
#include <stdlib.h>
#include <math.h>
#include <VersionHelpers.h>

#include "qcwin.h"
#include "qcwrt.h"
#include "support.h"
#include "rtsup.h"
#include "menus.h"

/******************************************************************************/

#define NUMBER_OF_DATA_CHANNELS        (8)  // maximum number of data channels
#define NT                             (NUMBER_OF_DATA_CHANNELS)
                                     
#define NUMBER_OF_PAGES                (8)
#define MAXIMUM_PAGE_NAME_LENGTH      (32)
                                   
#define PAGE_1_INDEX                   (0)
                                      
#define NUMBER_OF_STYLES               (8)
#define WINDOW_STYLE_DEFAULT          (0L)
#define WINDOW_POSITION_DEFAULT       (PAGE_FULL)
#define WINDOW_X_POSITION_DEFAULT      (0)
#define WINDOW_Y_POSITION_DEFAULT      (0)
#define WINDOW_X_WIDTH_DEFAULT         (0)
#define WINDOW_Y_WIDTH_DEFAULT         (0)
                                       
#define WINDOW_1_INDEX                 (0)
                                       
#define P1G1_X_MIN                     (0.005)
#define P1G1_Y_MIN                     (0.005)
#define P1G1_X_MAX                     (0.995)
#define P1G1_Y_MAX                     (0.995)
#define P1G1_GRAPH_COLOUR              (C_PALEBLUE)
#define P1G1_BORDER_COLOUR             (C_BLACK)
#define P1G1_BORDER_WIDTH              (1)

#define BASE_LINE_COLOUR               (1)
#define LAST_LINE_COLOUR              (15)
                                     
#define TIMER_1_EVENT_ID               (0)
#define TIMER_1_PERIOD               (100) // milliseconds
#define DATA_TIMER_INITIAL_VALUE       (0)
#define DATA_TIMER_ROLLOVER_VALUE   (1000)
#define DATA_TIMER_INTERVAL            (1)

#define P1G1_TAG_TANK_LEVEL          "Tank Level"
#define P1G1_TAG_STEAM_FLOW          "Steam Flow"
#define P1G1_TAG_PRODUCT_FLOW        "Product Flow"
#define P1G1_TAG_FEED_FLOW           "Feed Flow"
#define P1G1_TAG_COMPOSITION         "Composition"
#define P1G1_TAG_COLUMN_LEVEL        "Column Level"
#define P1G1_TAG_RECYCLE_FLOW        "Recycle Flow"
#define P1G1_TAG_REFLUX_FLOW         "Reflux Flow"
#define P1G1_TAG_NAMES               (8)
                                     
#define P1G1_TEXT_COLOUR             (C_GREEN)
#define P1G1_TEXT_FONT               "Arial"
#define P1G1_TEXT_SIZE                (16)
#define P1G1_TEXT_STYLE              (TEXT_BOLD | TEXT_ITAL)

#define P1G1_SAMPLING_RATE           (0.1)   // 100 milliseconds
#define P1G1_RELATIVE_RESET_INTERVAL (0.99)   // reset 1% ('smooth') of data display
#define P1G1_SET_POINT               (0.0)   // set-point alram
#define P1G1_HIGH_ALARM_LIMIT        (1.5)   // high alarm limit
#define P1G1_LOW_ALARM_LIMIT         (-1.75) // low alarm limit

#define P1G1_GRAPH_MAXIMUM_LIMIT     (2.0)
#define P1G1_GRAPH_MINIMUM_LIMIT     (-2.0)
#define P1G1_GRAPH_X_AXIS_RANGE      (10.0)
#define P1G1_GRAPH_SCROLLING_LINES   (GOD_SUBT_SCLINE)
#define P1G1_GRAPH_DECIMAL_PLACES    (2)
#define P1G1_GRAPH_Y_AXIS_TITLE      "seconds"
#define P1G1_GRAPH_PLOT_COLOUR       (C_WHITE)
#define P1G1_GRAPH_VALUES_AS_TEXT_N  (FALSE)
#define P1G1_GRAPH_VALUES_AS_TEXT    (TRUE)

/******************************************************************************/

typedef enum ddsHistoryBuffer_tTag
  {
  DDS_HISTORY_BUFFER_N,
  DDS_HISTORY_BUFFER
  } ddsHistoryBuffer_t;

typedef enum dataChannels_tTag
  {
  TANK_LEVEL = 0,
  STEAM_FLOW,
  PRODUCT_FLOW,
  FEED_FLOW,
  COMPOSITION,
  COLUMN_LEVEL,
  RECYCLE_FLOW,
  REFLUX_FLOW
  } dataChannels_t;

typedef struct windowStyle_tTag
  {
  DWORD windowStyle;
  int   windowPositionStyle;
  int   windowXposition;
  int   windowYposition;
  int   windowXwidth;
  int   windowYwidth;
  } windowStyle_t;

typedef struct pageData_tTag
  {
        LPCSTR         pageId;
        LPSTR          windowTitle;
        LPCSTR         pageWindowName;
        int            windowBackground;
        int            windowSizeControl;
  const windowStyle_t *pWindowStyle;
  } pageData_t;

/******************************************************************************/

HINSTANCE  hInst                = NULL;
HDATA      hData                = 0;
                                
PGRAPH_DEF pDynGrDesc           = NULL;   // graph descriptor pointer
TIMERPROC  lpTimerRoutine       = NULL;   // timer function pointer
int        idTimer              = 0;
int        timerCount           = DATA_TIMER_INITIAL_VALUE; // number of calls to the timer routine
LPCTSTR    iconLogoName         = "ICON";
LPCTSTR    mainMenuName         = "MainMenu";
LPCTSTR    szAppName            = "ScreenDemo";
LPCTSTR    qcDemoName           = "Quinn-Curtis Auto Scroll Demo";

const CHAR pageOne[]            = "PAGE_1";
      CHAR autoScroll[]         = { "Auto Scroll" };
const CHAR pageMenu[]           = "pageMenu";

const CHAR traceUnits[]         = "volts";

const char p1g1ChannelNames[P1G1_TAG_NAMES][TAGLEN] = { 
                                                       P1G1_TAG_TANK_LEVEL,
                                                       P1G1_TAG_STEAM_FLOW,
                                                       P1G1_TAG_PRODUCT_FLOW,
                                                       P1G1_TAG_FEED_FLOW,
                                                       P1G1_TAG_COMPOSITION,
                                                       P1G1_TAG_COLUMN_LEVEL,
                                                       P1G1_TAG_RECYCLE_FLOW,
                                                       P1G1_TAG_REFLUX_FLOW,
                                                       };

const windowStyle_t windowStyles[NUMBER_OF_STYLES] = { 
                                                        {
                                                          WINDOW_STYLE_DEFAULT,
                                                          WINDOW_POSITION_DEFAULT,
                                                          WINDOW_X_POSITION_DEFAULT,
                                                          WINDOW_Y_POSITION_DEFAULT,
                                                          WINDOW_X_WIDTH_DEFAULT,
                                                          WINDOW_Y_WIDTH_DEFAULT
                                                        }
                                                   };

pageData_t pages[NUMBER_OF_PAGES] = {
                                      { 
                                         &pageOne[0], // "PAGE 1",
                                          autoScroll, // "Auto Scroll",
                                         &pageMenu[0], // "Page Menu",
                                          C_LIGHTBLUE,
                                          MM_PROPORT,
                                        &windowStyles[WINDOW_1_INDEX]
                                      } 
                                  };

/******************************************************************************/

LRESULT CALLBACK _export MainWndProc(HWND   hwnd,
                                     UINT   message,
                                     WPARAM wParam,
                                     LPARAM lParam);
BOOL                     InitApplication(HINSTANCE hInstance);
BOOL                     InitInstance(HINSTANCE hInstance,
                                      INT       nCmdShow);
void    CALLBACK         StartGraphs1(PPAGE_DEF pPageDesc);
void    CALLBACK _export TimerRoutine(HWND  hwnd,
                                      UINT  msg,
                                      UINT  idTimer,
                                      DWORD dwTime);
void CALLBACK            DrawP1G1(PGRAPH_DEF pGrDesc, HDC hdc);

/******************************************************************************/

INT WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   PSTR      lpCmdLine, 
                   INT       nCmdShow)
  {
  /******************************************************************************/

  MSG msg;

  /******************************************************************************/

  if (!hPrevInstance)
    {
    if (!InitApplication(hInstance))
      {
      return(FALSE);
      }
    else
      {
      if (!InitInstance(hInstance, nCmdShow))
        {
        return(FALSE);
        }
      }
    }

  /******************************************************************************/

  // The Windows message loop
  while (GetMessage(&msg, NULL, 0, 0))
    {
    TranslateMessage(&msg);
    DispatchMessage(&msg);
    }

  /******************************************************************************/

  return(msg.wParam);

  /******************************************************************************/
  } /* end of WinMain                                                           */

/******************************************************************************/

BOOL InitApplication(HINSTANCE hInstance)
  {
  /******************************************************************************/

  WNDCLASS wc;

  /******************************************************************************/

  // Windows main window class definition
  wc.style         = CS_HREDRAW | CS_VREDRAW;
  wc.lpfnWndProc   = MainWndProc;
  wc.cbClsExtra    = 0;
  wc.cbWndExtra    = 0;
  wc.hInstance     = hInstance;
  wc.hIcon         = LoadIcon(hInstance, iconLogoName);
  wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
  wc.hbrBackground = (HBRUSH)GetStockObject(LTGRAY_BRUSH);
  wc.lpszMenuName  = mainMenuName;
  wc.lpszClassName = szAppName;

  /******************************************************************************/

  return(RegisterClass(&wc));

  /******************************************************************************/
  } /* end of InitApplication                                                   */

/******************************************************************************/

BOOL InitInstance(HINSTANCE hInstance, INT nCmdShow)
  {
  /******************************************************************************/

  HWND hwnd = NULL;

  /******************************************************************************/

  hInst = hInstance; // saves the current instance ???

  // Create the main window
  hwnd  = CreateWindow(szAppName,
                       qcDemoName,
                       (WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN),
                       CW_USEDEFAULT,                    
                       CW_USEDEFAULT,
                       CW_USEDEFAULT,
                       CW_USEDEFAULT,
                       NULL,
                       NULL,
                       hInstance,
                       NULL
                       );

  /******************************************************************************/

  if (!hwnd)
    {
    return(FALSE);
    }
  else
    {
    ShowWindow(hwnd, nCmdShow);
    UpdateWindow(hwnd);

    return(TRUE);
    }

  /******************************************************************************/
  } /* end of InitInstance                                                      */

/******************************************************************************/

LRESULT CALLBACK _export MainWndProc(HWND   hwnd,
                                     UINT   message,
                                     WPARAM wParam,
                                     LPARAM lParam)
  {
  /******************************************************************************/

    volatile PPAGE_DEF pageDesc = NULL;

  /******************************************************************************/

  /******************************************************************************/

  switch (message)
    {
    case WM_CREATE  : pageDesc = WGCreatePage(pages[PAGE_1_INDEX].pageId,
                                              hwnd,
                                              hInst,
                                              pages[PAGE_1_INDEX].windowTitle,
                                              StartGraphs1,
                                              pages[PAGE_1_INDEX].pageWindowName,
                                              pages[PAGE_1_INDEX].windowBackground,
                                              pages[PAGE_1_INDEX].windowSizeControl,
                                              pages[PAGE_1_INDEX].pWindowStyle->windowStyle,
                                              pages[PAGE_1_INDEX].pWindowStyle->windowPositionStyle,
                                              pages[PAGE_1_INDEX].pWindowStyle->windowXposition,
                                              pages[PAGE_1_INDEX].pWindowStyle->windowXposition,
                                              pages[PAGE_1_INDEX].pWindowStyle->windowXwidth,
                                              pages[PAGE_1_INDEX].pWindowStyle->windowYwidth
                                              );

                      lpTimerRoutine = MakeProcInstance((TIMERPROC)TimerRoutine, hInst);
                      idTimer        = SetTimer(NULL, TIMER_1_EVENT_ID, TIMER_1_PERIOD, lpTimerRoutine);

                      return(0);

                      break;
    case WM_CLOSE   : DestroyWindow(hwnd);
                      break;
    case WM_DESTROY : KillTimer(NULL, idTimer);
                      WRCleanup(TRUE);
                      PostQuitMessage(0);
                      FreeProcInstance(lpTimerRoutine);

                      return(0);

                      break;
    default         : return(DefWindowProc(hwnd, message, wParam, lParam));
    }

  /******************************************************************************/

  return(DefWindowProc(hwnd, message, wParam, lParam));

  /******************************************************************************/
  } /* end of MainWndProc                                                       */

/******************************************************************************/
/* Create a page-contained graph :                                            */
/******************************************************************************/

void CALLBACK StartGraphs1(PPAGE_DEF pPageDesc)
  {
/******************************************************************************/

  pDynGrDesc = WGCreateGraph(pPageDesc, 
                             DrawP1G1,
                             P1G1_X_MIN,
                             P1G1_Y_MIN,
                             P1G1_X_MAX,
                             P1G1_Y_MAX,
                             P1G1_GRAPH_COLOUR,
                             P1G1_BORDER_COLOUR,
                             P1G1_BORDER_WIDTH
    );

/******************************************************************************/
  } /* end of StartGraphs1                                                    */

/******************************************************************************/

void CALLBACK DrawP1G1(PGRAPH_DEF pGrDesc, HDC hdc)
  {
  /******************************************************************************/

  HGOBJ hScr[AS_NH]; // AS_NH is defined in 'Qcwrt.h'. This is used because the 
                     // function 'WRSetAutoScrollGraph' expects 'hScr' to be 
                     // of this length
  LPSTR    lpTags          = NULL;
  int      nLineColour[NT] = { 0, 0, 0, 0, 0, 0, 0, 0 };
  int      nTraces         = NT;
  int      lineColour      = 0;
  realtype rLow            = 0,
           rHigh           = 0,
           rSetp           = 0,
           rSampleInt      = 0,
           rRelResetInt    = 0;

  /******************************************************************************/

  // Allocate space for the tags and initialise them with the channel name
  if ((lpTags = (LPSTR)calloc(nTraces, TAGLEN)) != NULL)
    {
    int   tagNameIndex = 0;
    LPSTR lpTagsT      = NULL;

    for (tagNameIndex = 0; tagNameIndex < P1G1_TAG_NAMES; tagNameIndex++)
      {
      lpTagsT = lpTags + (tagNameIndex * TAGLEN);

      if (lstrcpy(lpTagsT, p1g1ChannelNames[tagNameIndex]) == NULL)
        {
        return;
        }
      }
    }
  else
    {
    return;
    }

  // Define a dynamic data set
  hData = WRDefineDynDataSet(autoScroll, // title of the data set
                             nTraces,    // the number of traces
                             traceUnits, // the trace units
                             lpTags,     // the trace names
                             DDS_HISTORY_BUFFER_N
                             );

  // Give the lines a colour
  for (lineColour = BASE_LINE_COLOUR; lineColour < nTraces; lineColour++)
    {
    nLineColour[lineColour] = (lineColour % LAST_LINE_COLOUR) + BASE_LINE_COLOUR;
    }

  WGSetTextByName(P1G1_TEXT_COLOUR, 
                  P1G1_TEXT_FONT,
                  P1G1_TEXT_SIZE,
                  P1G1_TEXT_STYLE);
    
   rSampleInt   = P1G1_SAMPLING_RATE;
   rRelResetInt = P1G1_RELATIVE_RESET_INTERVAL;
   rSetp        = P1G1_SET_POINT;
   rHigh        = P1G1_HIGH_ALARM_LIMIT;
   rLow         = P1G1_LOW_ALARM_LIMIT;

   // Create an auto-scrolling graph
   WRSetAutoScrollGraph(pGrDesc,                    // pointer to the graph descriptor
                        hData,                      // handle of the dynamic data set
                        rSampleInt,                 // data sampling innterval
                        rRelResetInt,               // relative data reset
                        P1G1_GRAPH_MINIMUM_LIMIT,   // minimum value
                        P1G1_GRAPH_MAXIMUM_LIMIT,   // maximum value
                        P1G1_GRAPH_X_AXIS_RANGE,    // independent variable range
                        P1G1_GRAPH_SCROLLING_LINES, // scrolling lines' format
                        P1G1_GRAPH_DECIMAL_PLACES,  // places after the decimal point
                        rSetp,                      // set-point
                        rHigh,                      // high alarm value
                        rLow,                       // low alarm value
                        P1G1_GRAPH_Y_AXIS_TITLE,    // dependent variable axis title
                        P1G1_GRAPH_PLOT_COLOUR,     // plotting area colour
                        nLineColour,                // data line colours
                        P1G1_GRAPH_VALUES_AS_TEXT,  // display dynamic values as text
                        hScr                        // object handles
                        );

   free(lpTags);

  /******************************************************************************/
  } /* end of DrawP1G1                                                          */

/******************************************************************************/

void CALLBACK _export TimerRoutine(HWND  hwnd,
                                   UINT  msg,
                                   UINT  idTimer,
                                   DWORD dwTime)
  {
  /******************************************************************************/

  realtype randomArgument = 0.0,
           rNewVals[NT]   = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
  int      randomValue    = 0;

  /******************************************************************************/

  if (WGIsDescValid(pDynGrDesc) == TRUE)
    {
    WGStart(); // always used in a timer routine if any charting functions are used

    // Compute the bais for some data randomisation
    randomArgument = M_PI * timerCount;
    randomValue    = rand(); // crappy 'random' function, but just for this demo...

    // Generate new values for the next data samples...can't be bothered to 
    // sanitise this crap from QCW, just live with it
    rNewVals[TANK_LEVEL]   = sin(randomArgument / (1025.0 - timerCount)) * 1.25;
    rNewVals[STEAM_FLOW]   = sin(randomArgument / 52.0) * (rand() / 50767.0) * 1.1 -
      1.25;
    rNewVals[PRODUCT_FLOW] = cos(randomArgument / 200.0) * (rand() / 60767.0) * 1.2 +
      1.05;
    rNewVals[FEED_FLOW]    = (randomValue % 4 != 0);
    rNewVals[FEED_FLOW]    = rand() / 32767.0 - 2.0;
    rNewVals[COMPOSITION]  = cos(randomArgument / 20.0) / 1.1;
    rNewVals[COLUMN_LEVEL] = 0.9 * cos(randomArgument / 7);
    rNewVals[RECYCLE_FLOW] = -1.35 - cos(randomArgument) / 10.0;
    rNewVals[REFLUX_FLOW]  = 1.7 + (cos(randomArgument / 5) + rNewVals[5] * 0.7) / 10.0;

    // Update the data set to the graph
    WRUpdateData(hData, rNewVals, pDynGrDesc);

    // Reset the random state
    timerCount = timerCount + DATA_TIMER_INTERVAL;
    
    if (timerCount > DATA_TIMER_ROLLOVER_VALUE)
      {
      timerCount = DATA_TIMER_INITIAL_VALUE;
      }
    }

  /******************************************************************************/
  } /* end of TimerRoutine                                                      */

/******************************************************************************/